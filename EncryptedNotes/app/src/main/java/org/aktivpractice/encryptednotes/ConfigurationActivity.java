package org.aktivpractice.encryptednotes;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import org.openintents.openpgp.util.OpenPgpAppPreference;
import org.openintents.openpgp.util.OpenPgpKeyPreference;

public class ConfigurationActivity extends PreferenceActivity {
    OpenPgpAppPreference mProvider;
    OpenPgpKeyPreference mKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.activity_configuration);

        Preference start = findPreference("start_activity");
        mProvider = (OpenPgpAppPreference) findPreference("openpgp_provider");
        mKey = (OpenPgpKeyPreference) findPreference("openpgp_key");

        start.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                setResult(Constants.RESULT_CODE_OK);
                finish();
                return true;
            }
        });

        mKey.setOpenPgpProvider(mProvider.getValue());
        mProvider.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                mKey.setOpenPgpProvider((String) o);
                return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mKey.handleOnActivityResult(requestCode, resultCode, data)) {
            return;
        }
    }
}
