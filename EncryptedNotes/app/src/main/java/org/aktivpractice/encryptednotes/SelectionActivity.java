package org.aktivpractice.encryptednotes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class SelectionActivity extends AppCompatActivity {
    private EditText mTitle;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<String> mFilesList;

    private SharedPreferences mConfig;
    private String mProviderPackageName;
    private long mKeyId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);

        reinitConfig();

        mTitle = (EditText) findViewById(R.id.editTitle);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mFilesList = findFiles();
        mAdapter = new MyAdapter(mFilesList);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void reinitConfig() {
        mConfig = PreferenceManager.getDefaultSharedPreferences(this);
        mProviderPackageName = mConfig.getString("openpgp_provider", "");
        mKeyId = mConfig.getLong("openpgp_key", 0);
    }

    private ArrayList<String> findFiles() {
        String[] files = fileList();
        return new ArrayList<String>(Arrays.asList(files));
    }

    public void openNote(View view) {
        if (isAllSet()) {
            String title = mTitle.getText().toString();
            if (Arrays.asList(fileList()).contains(title)) {
                open(title);
            } else {
                showPopup("Note with this title not found!");
            }
        }
    }

    public void deleteNote(View view) {
        String title = mTitle.getText().toString();
        if (Arrays.asList(fileList()).contains(title)) {
            delete(title);
        } else {
            showPopup("Note with this title not found!");
        }
    }

    public void createNote(View view) {
        if (isAllSet()) {
            create();
        }
    }

    public void editConfig(View view) {
        Intent data = new Intent(SelectionActivity.this, ConfigurationActivity.class);
        data.setAction(Constants.ACTION_EDIT_CONFIG);
        startActivityForResult(data, Constants.REQUEST_CODE_EDIT_CONFIG);
    }

    public void open(String title) {
        Intent data = new Intent(SelectionActivity.this, MainActivity.class);
        data.setAction(Constants.ACTION_OPEN);
        data.putExtra(Constants.EXTRA_NOTE_TITLE, title);
        startActivityForResult(data, Constants.REQUEST_CODE_OPEN);
    }

    private void delete(String file) {
        deleteFile(file);
        mFilesList.remove(file);
        mAdapter.notifyDataSetChanged();
    }

    private void create() {
        Intent data = new Intent(SelectionActivity.this, MainActivity.class);
        data.setAction(Constants.ACTION_CREATE);
        startActivityForResult(data, Constants.REQUEST_CODE_CREATE);
    }

    private boolean isAllSet() {
        if (TextUtils.isEmpty(mProviderPackageName)) {
            showPopup("Please select OpenPGP app!");
            return false;
        }
        else if (mKeyId == 0) {
            showPopup("Please select key!");
            return false;
        } else {
            return true;
        }
    }

    public void showPopup(final String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void addTitle(String title) {
        mFilesList.add(title);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case Constants.RESULT_CODE_OK: {
                reinitConfig();
                break;
            }
            case Constants.RESULT_CODE_SAVED: {
                switch (requestCode) {
                    case Constants.REQUEST_CODE_CREATE: {
                        addTitle(data.getStringExtra(Constants.EXTRA_NOTE_TITLE));
                        break;
                    }
                    case Constants.REQUEST_CODE_OPEN: {
                        String title = data.getStringExtra(Constants.EXTRA_NOTE_TITLE);
                        if (!mFilesList.contains(title)) {
                            delete(data.getStringExtra(Constants.EXTRA_NOTE_TITLE_OLD));
                            addTitle(title);
                        }
                        break;
                    }
                }
                break;
            }
            case Constants.RESULT_ERROR: {
                return;
            }
        }
    }
}
