package org.aktivpractice.encryptednotes;

public class Constants {
    public static final String TAG = "EncryptedNotes";
    public static final String ACTION_EDIT_CONFIG = "org.aktivpractice.ACTION_EDIT_CONFIG";
    public static final String ACTION_OPEN = "org.aktivpractice.ACTION_OPEN";
    public static final String ACTION_CREATE = "org.aktivpractice.ACTION_CREATE";
    public static final int REQUEST_CODE_EDIT_CONFIG = 8624;
    public static final int REQUEST_CODE_OPEN = 1357;
    public static final int REQUEST_CODE_CREATE = 7531;
    public static final int REQUEST_CODE_ENCRYPT = 8926;
    public static final int REQUEST_CODE_DECRYPT = 2689;
    public static final String EXTRA_NOTE_TITLE = "org.aktivpractice.EXTRA_NOTE_TITLE";
    public static final String EXTRA_NOTE_TITLE_OLD = "org.aktivpractice.EXTRA_NOTE_TITLE_OLD";
    public static final int RESULT_CODE_OK = 4692;
    public static final int RESULT_CODE_SAVED = 2468;
    public static final int RESULT_ERROR = 6666;
}
