package org.aktivpractice.encryptednotes;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.openintents.openpgp.IOpenPgpService2;
import org.openintents.openpgp.OpenPgpError;
import org.openintents.openpgp.util.OpenPgpApi;
import org.openintents.openpgp.util.OpenPgpServiceConnection;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    private Intent mIntent;
    private OpenPgpServiceConnection mServiceConnection;
    private long mKeyId;

    private EditText mTitle;
    private EditText mNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mIntent = getIntent();

        mTitle = (EditText) findViewById(R.id.editTitle);
        mNotes = (EditText) findViewById(R.id.editText);

        SharedPreferences config = PreferenceManager.getDefaultSharedPreferences(this);
        final String providerPackageName = config.getString("openpgp_provider", "");
        mKeyId = config.getLong("openpgp_key", 0);
        mServiceConnection = new OpenPgpServiceConnection(this, providerPackageName,
                new OpenPgpServiceConnection.OnBound() {
                    @Override
                    public void onBound(IOpenPgpService2 service) {
                        Log.d(Constants.TAG, "Bound to " + providerPackageName + "!");
                        if (mIntent.getAction().equals(Constants.ACTION_OPEN)) {
                            decrypt(new Intent());
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e(Constants.TAG, "Exception when binding!");
                    }
                });
        mServiceConnection.bindToService();
    }

    private class MyCallback implements OpenPgpApi.IOpenPgpCallback {
        ByteArrayOutputStream os;
        int requestCode;

        private MyCallback(ByteArrayOutputStream os, int requestCode) {
            this.os = os;
            this.requestCode = requestCode;
        }

        @Override
        public  void onReturn(Intent result) {
            switch (result.getIntExtra(OpenPgpApi.RESULT_CODE, OpenPgpApi.RESULT_CODE_ERROR)) {
                case OpenPgpApi.RESULT_CODE_SUCCESS: {
                    if (os != null) {
                        switch (requestCode) {
                            case Constants.REQUEST_CODE_ENCRYPT: {
                                showPopup("Encryption succeed!");
                                try {
                                    saveToFile(os.toString("UTF-8"));
                                } catch (UnsupportedEncodingException e) {
                                    Log.e(Constants.TAG, "UnsupportedEncodingException", e);
                                } catch (IOException e) {
                                    Log.e(Constants.TAG, "IOException", e);
                                }
                                break;
                            }
                            case Constants.REQUEST_CODE_DECRYPT: {
                                showPopup("Decryption succeed!");
                                try {
                                    mNotes.setText(os.toString("UTF-8"));
                                    mTitle.setText(mIntent.getStringExtra(Constants.EXTRA_NOTE_TITLE));
                                } catch (UnsupportedEncodingException e) {
                                    Log.e(Constants.TAG, "UnsupportedEncodingException", e);
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
                case OpenPgpApi.RESULT_CODE_USER_INTERACTION_REQUIRED: {
                    showPopup("User interaction required!");
                    PendingIntent pi = result.getParcelableExtra(OpenPgpApi.RESULT_INTENT);
                    try {
                        startIntentSenderForResult(pi.getIntentSender(), requestCode, null, 0, 0, 0);
                    } catch (IntentSender.SendIntentException e) {
                        Log.e(Constants.TAG, "SendIntentException", e);
                    }
                    break;
                }
                case OpenPgpApi.RESULT_CODE_ERROR: {
                    showPopup("Error occurred!");
                    OpenPgpError error = result.getParcelableExtra(OpenPgpApi.RESULT_ERROR);
                    handleError(error);
                    break;
                }
            }
        }
    }

    private void saveToFile(String text) throws IOException {
        String title = mTitle.getText().toString();
        Intent data = new Intent();
        if (mIntent.getAction().equals(Constants.ACTION_OPEN)) {
            String oldTitle = mIntent.getStringExtra(Constants.EXTRA_NOTE_TITLE);
            if (oldTitle.equals(title)) {
                deleteFile(oldTitle);
            }
            data.putExtra(Constants.EXTRA_NOTE_TITLE_OLD, oldTitle);
        }
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(title, Context.MODE_PRIVATE);
            fos.write(text.getBytes());
            fos.close();
            data.putExtra(Constants.EXTRA_NOTE_TITLE, title);
            setResult(Constants.RESULT_CODE_SAVED, data);
            finish();
        } catch (FileNotFoundException e) {}
        catch (IOException e) {
            Log.e(Constants.TAG, "IOException", e);
        }
        finally {
            if (fos != null) {
                fos.close();
            }
        }
    }

    private void handleError(final OpenPgpError error) {
        showPopup("Error ID: " + error.getErrorId() + "\n" + error.getMessage());
        Log.e(Constants.TAG, "ErrorId: " + error.getErrorId());
        Log.e(Constants.TAG, "Message: " + error.getMessage());
        setResult(Constants.RESULT_ERROR);
        finish();
    }

    public void clearNote(View view) {
        mNotes.setText("");
    }

    public void saveNote(View view) {
        if (TextUtils.isEmpty(mTitle.getText().toString())) {
            showPopup("Enter note's title!");
            return;
        } else if (TextUtils.isEmpty(mNotes.getText().toString())) {
            showPopup("Nothing to save, note is empty!");
            return;
        } else if ((mIntent.getAction().equals(Constants.ACTION_CREATE)) &&
                (Arrays.asList(fileList()).contains(mTitle.getText().toString()))) {
            showPopup("Note with this title already exist!");
            return;
        } else {
            encrypt(new Intent());
        }
    }

    private void encrypt(Intent data) {
        data.setAction(OpenPgpApi.ACTION_ENCRYPT);
        data.putExtra(OpenPgpApi.EXTRA_KEY_IDS, new long[]{mKeyId});
        data.putExtra(OpenPgpApi.EXTRA_REQUEST_ASCII_ARMOR, true);

        InputStream is = getInputstream(false);
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        OpenPgpApi api = new OpenPgpApi(this, mServiceConnection.getService());
        api.executeApiAsync(data, is, os, new MyCallback(os, Constants.REQUEST_CODE_ENCRYPT));
    }

    private void decrypt(Intent data) {
        data.setAction(OpenPgpApi.ACTION_DECRYPT_VERIFY);

        InputStream is = getInputstream(true);
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        OpenPgpApi api = new OpenPgpApi(this, mServiceConnection.getService());
        api.executeApiAsync(data, is, os, new MyCallback(os, Constants.REQUEST_CODE_DECRYPT));
    }

    private InputStream getInputstream(boolean fromFile) {
        InputStream is = null;
        if (fromFile) {
            String title = mIntent.getStringExtra(Constants.EXTRA_NOTE_TITLE);
            try {
                FileInputStream fin = openFileInput(title);
                byte[] bytes = new byte[fin.available()];
                fin.read(bytes);
                fin.close();
                is = new ByteArrayInputStream(bytes);
            } catch (FileNotFoundException e) {
                Log.e(Constants.TAG, "File '" + title + "' not found!", e);
            } catch (IOException e) {
                Log.e(Constants.TAG, "Error with fin", e);
            }
        } else {
            try {
                String inputStr = mNotes.getText().toString();
                is = new ByteArrayInputStream(inputStr.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                Log.e(Constants.TAG, "UnsupportedEncodingException", e);
            }
        }
        return is;
    }

    public void showPopup(final String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode)
            {
                case Constants.REQUEST_CODE_ENCRYPT: {
                    encrypt(data);
                    break;
                }
                case Constants.REQUEST_CODE_DECRYPT: {
                    decrypt(data);
                    break;
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mServiceConnection != null) {
            mServiceConnection.unbindFromService();
        }
    }
}
